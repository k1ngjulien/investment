#!/usr/bin/python3

import argparse
import matplotlib
import matplotlib.pyplot as plt
import numpy as np


parser = argparse.ArgumentParser(description='Calculate Investment returns')
parser.add_argument('-r', dest='returns', type=int, required=True,
                    help='annual returns (%% p.a.))')
parser.add_argument('-i', dest='monthly', type=int, required=True,
                    help='monthly investment (€)')
parser.add_argument('-a', dest='increase', type=int,  default=0,
                    help='annual monthyl investment increase')
parser.add_argument('-t', dest='runtime', type=int,
                    required=True, help='runtime (years)')
parser.add_argument('-b', dest='balance', type=int,  default=0,
                    help='starting balance (€)')
parser.add_argument('--tax', dest='tax', type=float,  default=27.5,
                    help='capital gains tax (%%p.a.), default 27.5%')

args = parser.parse_args()

annual_returns = args.returns
start_balance = args.balance
monthly_investment = args.monthly
runtime = args.runtime

yearly = monthly_investment * 12

balance = start_balance
balance_history = []
capital = start_balance
capital_history = []
gain_history = []
for i in range(runtime):
    increase = balance * (1 + (annual_returns / 100)) - balance
    increase *= 1 - (args.tax / 100)
    balance += increase
    balance += yearly

    capital += yearly

    yearly += args.increase * 12
    total_gain = 1 - (capital / balance)

    balance_history.append(balance)
    capital_history.append(capital)
    gain_history.append(total_gain)


print(f'          no interest\twith interest\tTotal interest gain')
for i, (b, c, g) in enumerate(zip(balance_history, capital_history, gain_history)):
    print(
        f'Year ({i}): {int(b)} €\t{int(c)} €\t{round(100 * g, 3)}%')


# Data for plotting

fig, ax = plt.subplots()
ax.plot(range(args.runtime), balance_history, color='tab:red')
ax.plot(range(args.runtime), capital_history, color='tab:green')

ax.set(xlabel='time (years)', ylabel='balance (€)',
       title=f'({args.returns}%p.a. | {args.monthly}€/month | {args.increase}€/month/year increase)')

ax.grid()

gain_history = [g * 100 for g in gain_history]

ax2 = ax.twinx()
ax2.plot(range(args.runtime), gain_history, color='tab:blue')
ax2.set(ylabel='% gain total p.a. due to compound interest')

fig.tight_layout()


plt.show()
