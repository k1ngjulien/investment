
function updatePlot() {
  const returns = parseInt(document.getElementById('returns').value)
  const monthly = parseInt(document.getElementById('monthly').value)
  const increase = parseInt(document.getElementById('increase').value)
  const startingBalance = parseInt(document.getElementById('balance').value)
  const runtime = parseInt(document.getElementById('runtime').value)
  const tax = parseFloat(document.getElementById('tax').value)

  let yearly = monthly * 12;
  let balance = startingBalance;
  let balanceHistory = [];
  let capital = startingBalance;
  let capitalHistory = [];
  let gainHistory = [];

  for(let i = 0; i < runtime; i++) {
    // add yearly savings
    balance += yearly;
    capital += yearly;

    // calculate returns
    let gain = balance * (1 + (returns / 100)) - balance

    // remove tax
    gain *= 1 - (tax / 100);

    balance += gain;

    yearly += increase * 12;

    let relativeGain = 1 - (capital / balance);

    balanceHistory.push(balance);
    capitalHistory.push(capital);
    gainHistory.push(relativeGain);
  }


  const plot = document.getElementById('plot');
  const axis = [];
  const startingYear = new Date().getFullYear();
  for(let i = 0; i < runtime; i++) {
    axis.push(startingYear + i);
  }

  const trace1 = {
      x: axis,
      y: balanceHistory,
      name: 'Balance'
  }

  const trace2 = {
      x: axis,
      y: capitalHistory,
      name: 'invested Capital'
  }

  const trace3 = {
      x: axis,
      y: gainHistory.map(v => v * 100),
      yaxis: 'y2',
      name: '% Gain'
  }

  const data = [ trace1, trace2, trace3 ];

  const layout = {
    yaxis: { title: '€' },
    yaxis2: {
      title: '%',
      overlaying: 'y',
      side: 'right'
    },
  };

  var config = {responsive: true}

  Plotly.newPlot( plot, data, layout, config );
}

updatePlot();

document.getElementById('calculate').addEventListener('click', () => updatePlot());
